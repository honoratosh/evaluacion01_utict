/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package unam.utict.piramide;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author honorato
 */
public class PiramideTest {
    
    public PiramideTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    @Test
    public void testGenerarPatron() {
        System.out.println("generarPatron");
        Piramide instance = new Piramide();
        
        int numero = 1;
        String expResult = "1";
        String result = instance.generarPatron(numero);
        assertEquals(expResult, result);
        
        numero = 2;
        expResult = "1\n22";
        result = instance.generarPatron(numero);
        assertEquals(expResult, result);
        
        numero = 3;
        expResult = "1\n22\n333";
        result = instance.generarPatron(numero);
        assertEquals(expResult, result);
        
        numero = 10;
        expResult = "1\n22\n333\n4444\n55555\n666666\n7777777\n88888888\n999999999\n10101010101010101010";
        result = instance.generarPatron(numero);
        assertEquals(expResult, result);
        
    }
    
}
