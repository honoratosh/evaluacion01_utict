/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package unam.utict.maximomultiplo;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author honorato
 */
public class MaximoMultiploTest {
    
    public MaximoMultiploTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    @Test
    public void testgenerarMaximoMultiplo() {
        System.out.println("generarMaximoMultiplo");
        MaximoMultiplo instance = new MaximoMultiplo();
        
        int divisor = 2;
        int limite = 5;
        int expResult = 4;
        int result = instance.generarMaximoMultiplo(divisor, limite);
        assertEquals(expResult, result);
        
        divisor = 5;
        limite = 30;
        expResult = 30;
        result = instance.generarMaximoMultiplo(divisor, limite);
        assertEquals(expResult, result);
        
    }
    
}
