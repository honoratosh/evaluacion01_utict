/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package unam.utict.interseccion;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author honorato
 */
public class InterseccionTest {
    
    public InterseccionTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of encontrar method, of class Interseccion.
     */
    @Test
    public void testEncontrar() {
        System.out.println("encontrar");
        int[] primero = {5,9,2,4,6,0};
        int[] segundo = {4,2,7,8,4,5,0};
        Interseccion instance = new Interseccion();
        int[] expResult = {5,2,4,0};
        int[] result = instance.encontrar(primero, segundo);
        assertArrayEquals(expResult, result);
        
        int[] primero2 = {22,89,4,10,33};
        int[] segundo2 = {1,2,9};
        int[] expResult2 = {};
        result = instance.encontrar(primero2, segundo2);
        assertArrayEquals(expResult2, result);        

    }
    
}
