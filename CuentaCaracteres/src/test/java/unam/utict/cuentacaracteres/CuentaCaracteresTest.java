/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package unam.utict.cuentacaracteres;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author honorato
 */
public class CuentaCaracteresTest {
    
    public CuentaCaracteresTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of contar method, of class CuentaCaracteres.
     */
    @Test
    public void testContar() {
        System.out.println("contar");
        String cadena = "alma";
        CuentaCaracteres instance = new CuentaCaracteres();
        int[][] expResult = {{97,2},{108,1},{109,1}};
        int[][] result = instance.contar(cadena);
        assertArrayEquals(expResult, result);

        cadena = "murcielago";
        int[][] expResult2 = {{109,1},{117,1},{114,1},{99,1},{105,1},{101,1},
                              {108,1},{97,1},{103,1},{111,1}};
        result = instance.contar(cadena);
        assertArrayEquals(expResult2, result);

    }
    
}
