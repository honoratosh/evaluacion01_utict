/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package unam.utict.palindromo;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author honorato
 */
public class PalindromoTest {
    
    public PalindromoTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of verificarPalindromo method, of class Palindromo.
     */
    @Test
    public void testVerificarPalindromo() {
        System.out.println("verificarPalindromo");
        String cadena = "";
        Palindromo instance = new Palindromo();
        Boolean expResult = Boolean.FALSE;
        Boolean result = instance.verificarPalindromo(cadena);
        assertEquals(expResult, result);

        
        cadena = "anita lava la tina";
        expResult = Boolean.TRUE;
        result = instance.verificarPalindromo(cadena);
        assertEquals(expResult, result);
        
        cadena = "A Mafalda dad la fama";
        expResult = Boolean.TRUE;
        result = instance.verificarPalindromo(cadena);
        assertEquals(expResult, result);
        
        
    }
    
}
