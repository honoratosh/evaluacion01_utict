/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package unam.utict.autobus;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author honorato
 */
public class AutobusTest {
    
    public AutobusTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of calcularPasajeros method, of class Autobus.
     */
    @Test
    public void testCalcularPasajeros() {
        System.out.println("calcularPasajeros");
        int[][] cambios =  {{3, 0},
                            {5, 1},
                            {2, 1},
                            {4, 3},
                            {0, 8}};
        Autobus instance = new Autobus();
        int expResult = 1;
        int result = instance.calcularPasajeros(cambios);
        assertEquals(expResult, result);

        int[][] cambios2 = {{1, 0},
                            {1, 0},
                            {5, 1},
                            {0, 3},
                            {1, 1}};
        expResult = 3;
        result = instance.calcularPasajeros(cambios2);
        assertEquals(expResult, result);
        
    }

}
