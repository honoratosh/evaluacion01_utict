/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package unam.utict.factorial;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author honorato
 */
public class FactorialTest {
    
    public FactorialTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of calcularFactorial method, of class Factorial.
     */
    @Test
    public void testCalcularFactorial() {
        System.out.println("calcularFactorial");
        int n = 0;
        Factorial instance = new Factorial();
        long expResult = 1L;
        long result = instance.calcularFactorial(n);
        assertEquals(expResult, result);

        n = 1;
        expResult = 1L;
        result = instance.calcularFactorial(n);
        assertEquals(expResult, result);
        
        n = 2;
        expResult = 2L;
        result = instance.calcularFactorial(n);
        assertEquals(expResult, result);
        
        n = 3;
        expResult = 6L;
        result = instance.calcularFactorial(n);
        assertEquals(expResult, result);
        
        n = 11;
        expResult = 39916800L;
        result = instance.calcularFactorial(n);
        assertEquals(expResult, result);

    }
    
}
