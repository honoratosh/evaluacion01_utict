/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package unam.utict.cerouno;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author honorato
 */
public class CeroUnoTest {
    
    public CeroUnoTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of validarCadena method, of class CeroUno.
     */
    @Test
    public void testValidarCadena() {
        System.out.println("validarCadena");
        CeroUno instance = new CeroUno();
        
        String cadena = "";
        Boolean expResult = Boolean.FALSE;
        Boolean result = instance.validarCadena(cadena);
        assertEquals(expResult, result);
        
        cadena = "01";
        expResult = Boolean.TRUE;
        result = instance.validarCadena(cadena);
        assertEquals(expResult, result);
        
        cadena = "0011";
        expResult = Boolean.TRUE;
        result = instance.validarCadena(cadena);
        assertEquals(expResult, result);
        
        cadena = "000111";
        expResult = Boolean.TRUE;
        result = instance.validarCadena(cadena);
        assertEquals(expResult, result);

        cadena = "011";
        expResult = Boolean.FALSE;
        result = instance.validarCadena(cadena);
        assertEquals(expResult, result);
        
    }
    
}
