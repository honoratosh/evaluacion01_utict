/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package unam.utict.elementomenor;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author honorato
 */
public class ElementoMenorTest {
    
    public ElementoMenorTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of encontrarElementoMenor method, of class ElementoMenor.
     */
    @Test
    public void testEncontrarElementoMenor() {
        System.out.println("encontrarElementoMenor");
        int[] arreglo = {7,6,5,4,3,2};
        ElementoMenor instance = new ElementoMenor();
        int expResult = 2;
        int result = instance.encontrarElementoMenor(arreglo);
        assertEquals(expResult, result);

        int[] arreglo2 = {-3,0,2,44,7,2};
        expResult = -3;
        result = instance.encontrarElementoMenor(arreglo2);
        assertEquals(expResult, result);

    }
    
}
